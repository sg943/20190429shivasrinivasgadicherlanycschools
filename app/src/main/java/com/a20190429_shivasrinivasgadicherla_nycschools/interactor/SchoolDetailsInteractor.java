package com.a20190429_shivasrinivasgadicherla_nycschools.interactor;

import com.a20190429_shivasrinivasgadicherla_nycschools.model.SchoolDetails;

import java.util.List;

/**
 * Created by srini on 4/30/19.
 */

public interface SchoolDetailsInteractor {
    void setData(SchoolDetails schoolDetails);

    void showError();
}
