package com.a20190429_shivasrinivasgadicherla_nycschools.interactor;

import com.a20190429_shivasrinivasgadicherla_nycschools.model.School;

import java.util.List;

/**
 * Created by srini on 4/30/19.
 */

public interface SchoolsActivityInteractor {
    void setData(List<School> schoolsList);

    void showError();
}
