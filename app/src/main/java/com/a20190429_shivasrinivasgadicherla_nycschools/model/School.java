package com.a20190429_shivasrinivasgadicherla_nycschools.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by srini on 4/30/19.
 */

public class School implements Parcelable {
    @SerializedName("academicopportunities1")
    @Expose
    private String academicopportunities1;
    @SerializedName("academicopportunities2")
    @Expose
    private String academicopportunities2;
    @SerializedName("admissionspriority11")
    @Expose
    private String admissionspriority11;
    @SerializedName("admissionspriority21")
    @Expose
    private String admissionspriority21;
    @SerializedName("admissionspriority31")
    @Expose
    private String admissionspriority31;
    @SerializedName("attendance_rate")
    @Expose
    private String attendanceRate;
    @SerializedName("bbl")
    @Expose
    private String bbl;
    @SerializedName("bin")
    @Expose
    private String bin;
    @SerializedName("boro")
    @Expose
    private String boro;
    @SerializedName("borough")
    @Expose
    private String borough;
    @SerializedName("building_code")
    @Expose
    private String buildingCode;
    @SerializedName("bus")
    @Expose
    private String bus;
    @SerializedName("census_tract")
    @Expose
    private String censusTract;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("code1")
    @Expose
    private String code1;
    @SerializedName("community_board")
    @Expose
    private String communityBoard;
    @SerializedName("council_district")
    @Expose
    private String councilDistrict;
    @SerializedName("dbn")
    @Expose
    private String dbn;
    @SerializedName("directions1")
    @Expose
    private String directions1;
    @SerializedName("ell_programs")
    @Expose
    private String ellPrograms;
    @SerializedName("extracurricular_activities")
    @Expose
    private String extracurricularActivities;
    @SerializedName("fax_number")
    @Expose
    private String faxNumber;
    @SerializedName("finalgrades")
    @Expose
    private String finalgrades;
    @SerializedName("grade9geapplicants1")
    @Expose
    private String grade9geapplicants1;
    @SerializedName("grade9geapplicantsperseat1")
    @Expose
    private String grade9geapplicantsperseat1;
    @SerializedName("grade9gefilledflag1")
    @Expose
    private String grade9gefilledflag1;
    @SerializedName("grade9swdapplicants1")
    @Expose
    private String grade9swdapplicants1;
    @SerializedName("grade9swdapplicantsperseat1")
    @Expose
    private String grade9swdapplicantsperseat1;
    @SerializedName("grade9swdfilledflag1")
    @Expose
    private String grade9swdfilledflag1;
    @SerializedName("grades2018")
    @Expose
    private String grades2018;
    @SerializedName("interest1")
    @Expose
    private String interest1;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("method1")
    @Expose
    private String method1;
    @SerializedName("neighborhood")
    @Expose
    private String neighborhood;
    @SerializedName("nta")
    @Expose
    private String nta;
    @SerializedName("offer_rate1")
    @Expose
    private String offerRate1;
    @SerializedName("overview_paragraph")
    @Expose
    private String overviewParagraph;
    @SerializedName("pct_stu_enough_variety")
    @Expose
    private String pctStuEnoughVariety;
    @SerializedName("pct_stu_safe")
    @Expose
    private String pctStuSafe;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("primary_address_line_1")
    @Expose
    private String primaryAddressLine1;
    @SerializedName("program1")
    @Expose
    private String program1;
    @SerializedName("requirement1_1")
    @Expose
    private String requirement11;
    @SerializedName("requirement2_1")
    @Expose
    private String requirement21;
    @SerializedName("requirement3_1")
    @Expose
    private String requirement31;
    @SerializedName("requirement4_1")
    @Expose
    private String requirement41;
    @SerializedName("requirement5_1")
    @Expose
    private String requirement51;
    @SerializedName("school_10th_seats")
    @Expose
    private String school10thSeats;
    @SerializedName("school_accessibility_description")
    @Expose
    private String schoolAccessibilityDescription;
    @SerializedName("school_email")
    @Expose
    private String schoolEmail;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("school_sports")
    @Expose
    private String schoolSports;
    @SerializedName("seats101")
    @Expose
    private String seats101;
    @SerializedName("seats9ge1")
    @Expose
    private String seats9ge1;
    @SerializedName("seats9swd1")
    @Expose
    private String seats9swd1;
    @SerializedName("state_code")
    @Expose
    private String stateCode;
    @SerializedName("subway")
    @Expose
    private String subway;
    @SerializedName("total_students")
    @Expose
    private String totalStudents;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("academicopportunities3")
    @Expose
    private String academicopportunities3;
    @SerializedName("addtl_info1")
    @Expose
    private String addtlInfo1;
    @SerializedName("eligibility1")
    @Expose
    private String eligibility1;
    @SerializedName("language_classes")
    @Expose
    private String languageClasses;
    @SerializedName("transfer")
    @Expose
    private String transfer;
    @SerializedName("academicopportunities4")
    @Expose
    private String academicopportunities4;
    @SerializedName("academicopportunities5")
    @Expose
    private String academicopportunities5;
    @SerializedName("college_career_rate")
    @Expose
    private String collegeCareerRate;
    @SerializedName("diplomaendorsements")
    @Expose
    private String diplomaendorsements;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("girls")
    @Expose
    private String girls;
    @SerializedName("graduation_rate")
    @Expose
    private String graduationRate;
    @SerializedName("psal_sports_boys")
    @Expose
    private String psalSportsBoys;
    @SerializedName("psal_sports_coed")
    @Expose
    private String psalSportsCoed;
    @SerializedName("psal_sports_girls")
    @Expose
    private String psalSportsGirls;
    @SerializedName("shared_space")
    @Expose
    private String sharedSpace;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("advancedplacement_courses")
    @Expose
    private String advancedplacementCourses;
    @SerializedName("campus_name")
    @Expose
    private String campusName;
    @SerializedName("prgdesc1")
    @Expose
    private String prgdesc1;
    @SerializedName("admissionspriority41")
    @Expose
    private String admissionspriority41;
    @SerializedName("admissionspriority12")
    @Expose
    private String admissionspriority12;
    @SerializedName("code2")
    @Expose
    private String code2;
    @SerializedName("grade9geapplicants2")
    @Expose
    private String grade9geapplicants2;
    @SerializedName("grade9geapplicantsperseat2")
    @Expose
    private String grade9geapplicantsperseat2;
    @SerializedName("grade9gefilledflag2")
    @Expose
    private String grade9gefilledflag2;
    @SerializedName("grade9swdapplicants2")
    @Expose
    private String grade9swdapplicants2;
    @SerializedName("grade9swdapplicantsperseat2")
    @Expose
    private String grade9swdapplicantsperseat2;
    @SerializedName("grade9swdfilledflag2")
    @Expose
    private String grade9swdfilledflag2;
    @SerializedName("interest2")
    @Expose
    private String interest2;
    @SerializedName("method2")
    @Expose
    private String method2;
    @SerializedName("prgdesc2")
    @Expose
    private String prgdesc2;
    @SerializedName("program2")
    @Expose
    private String program2;
    @SerializedName("seats102")
    @Expose
    private String seats102;
    @SerializedName("seats9ge2")
    @Expose
    private String seats9ge2;
    @SerializedName("seats9swd2")
    @Expose
    private String seats9swd2;

    public String getAcademicopportunities1() {
        return academicopportunities1;
    }

    public void setAcademicopportunities1(String academicopportunities1) {
        this.academicopportunities1 = academicopportunities1;
    }

    public String getAcademicopportunities2() {
        return academicopportunities2;
    }

    public void setAcademicopportunities2(String academicopportunities2) {
        this.academicopportunities2 = academicopportunities2;
    }

    public String getAdmissionspriority11() {
        return admissionspriority11;
    }

    public void setAdmissionspriority11(String admissionspriority11) {
        this.admissionspriority11 = admissionspriority11;
    }

    public String getAdmissionspriority21() {
        return admissionspriority21;
    }

    public void setAdmissionspriority21(String admissionspriority21) {
        this.admissionspriority21 = admissionspriority21;
    }

    public String getAdmissionspriority31() {
        return admissionspriority31;
    }

    public void setAdmissionspriority31(String admissionspriority31) {
        this.admissionspriority31 = admissionspriority31;
    }

    public String getAttendanceRate() {
        return attendanceRate;
    }

    public void setAttendanceRate(String attendanceRate) {
        this.attendanceRate = attendanceRate;
    }

    public String getBbl() {
        return bbl;
    }

    public void setBbl(String bbl) {
        this.bbl = bbl;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getBoro() {
        return boro;
    }

    public void setBoro(String boro) {
        this.boro = boro;
    }

    public String getBorough() {
        return borough;
    }

    public void setBorough(String borough) {
        this.borough = borough;
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public void setBuildingCode(String buildingCode) {
        this.buildingCode = buildingCode;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getCensusTract() {
        return censusTract;
    }

    public void setCensusTract(String censusTract) {
        this.censusTract = censusTract;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCode1() {
        return code1;
    }

    public void setCode1(String code1) {
        this.code1 = code1;
    }

    public String getCommunityBoard() {
        return communityBoard;
    }

    public void setCommunityBoard(String communityBoard) {
        this.communityBoard = communityBoard;
    }

    public String getCouncilDistrict() {
        return councilDistrict;
    }

    public void setCouncilDistrict(String councilDistrict) {
        this.councilDistrict = councilDistrict;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getDirections1() {
        return directions1;
    }

    public void setDirections1(String directions1) {
        this.directions1 = directions1;
    }

    public String getEllPrograms() {
        return ellPrograms;
    }

    public void setEllPrograms(String ellPrograms) {
        this.ellPrograms = ellPrograms;
    }

    public String getExtracurricularActivities() {
        return extracurricularActivities;
    }

    public void setExtracurricularActivities(String extracurricularActivities) {
        this.extracurricularActivities = extracurricularActivities;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getFinalgrades() {
        return finalgrades;
    }

    public void setFinalgrades(String finalgrades) {
        this.finalgrades = finalgrades;
    }

    public String getGrade9geapplicants1() {
        return grade9geapplicants1;
    }

    public void setGrade9geapplicants1(String grade9geapplicants1) {
        this.grade9geapplicants1 = grade9geapplicants1;
    }

    public String getGrade9geapplicantsperseat1() {
        return grade9geapplicantsperseat1;
    }

    public void setGrade9geapplicantsperseat1(String grade9geapplicantsperseat1) {
        this.grade9geapplicantsperseat1 = grade9geapplicantsperseat1;
    }

    public String getGrade9gefilledflag1() {
        return grade9gefilledflag1;
    }

    public void setGrade9gefilledflag1(String grade9gefilledflag1) {
        this.grade9gefilledflag1 = grade9gefilledflag1;
    }

    public String getGrade9swdapplicants1() {
        return grade9swdapplicants1;
    }

    public void setGrade9swdapplicants1(String grade9swdapplicants1) {
        this.grade9swdapplicants1 = grade9swdapplicants1;
    }

    public String getGrade9swdapplicantsperseat1() {
        return grade9swdapplicantsperseat1;
    }

    public void setGrade9swdapplicantsperseat1(String grade9swdapplicantsperseat1) {
        this.grade9swdapplicantsperseat1 = grade9swdapplicantsperseat1;
    }

    public String getGrade9swdfilledflag1() {
        return grade9swdfilledflag1;
    }

    public void setGrade9swdfilledflag1(String grade9swdfilledflag1) {
        this.grade9swdfilledflag1 = grade9swdfilledflag1;
    }

    public String getGrades2018() {
        return grades2018;
    }

    public void setGrades2018(String grades2018) {
        this.grades2018 = grades2018;
    }

    public String getInterest1() {
        return interest1;
    }

    public void setInterest1(String interest1) {
        this.interest1 = interest1;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMethod1() {
        return method1;
    }

    public void setMethod1(String method1) {
        this.method1 = method1;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getNta() {
        return nta;
    }

    public void setNta(String nta) {
        this.nta = nta;
    }

    public String getOfferRate1() {
        return offerRate1;
    }

    public void setOfferRate1(String offerRate1) {
        this.offerRate1 = offerRate1;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public void setOverviewParagraph(String overviewParagraph) {
        this.overviewParagraph = overviewParagraph;
    }

    public String getPctStuEnoughVariety() {
        return pctStuEnoughVariety;
    }

    public void setPctStuEnoughVariety(String pctStuEnoughVariety) {
        this.pctStuEnoughVariety = pctStuEnoughVariety;
    }

    public String getPctStuSafe() {
        return pctStuSafe;
    }

    public void setPctStuSafe(String pctStuSafe) {
        this.pctStuSafe = pctStuSafe;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPrimaryAddressLine1() {
        return primaryAddressLine1;
    }

    public void setPrimaryAddressLine1(String primaryAddressLine1) {
        this.primaryAddressLine1 = primaryAddressLine1;
    }

    public String getProgram1() {
        return program1;
    }

    public void setProgram1(String program1) {
        this.program1 = program1;
    }

    public String getRequirement11() {
        return requirement11;
    }

    public void setRequirement11(String requirement11) {
        this.requirement11 = requirement11;
    }

    public String getRequirement21() {
        return requirement21;
    }

    public void setRequirement21(String requirement21) {
        this.requirement21 = requirement21;
    }

    public String getRequirement31() {
        return requirement31;
    }

    public void setRequirement31(String requirement31) {
        this.requirement31 = requirement31;
    }

    public String getRequirement41() {
        return requirement41;
    }

    public void setRequirement41(String requirement41) {
        this.requirement41 = requirement41;
    }

    public String getRequirement51() {
        return requirement51;
    }

    public void setRequirement51(String requirement51) {
        this.requirement51 = requirement51;
    }

    public String getSchool10thSeats() {
        return school10thSeats;
    }

    public void setSchool10thSeats(String school10thSeats) {
        this.school10thSeats = school10thSeats;
    }

    public String getSchoolAccessibilityDescription() {
        return schoolAccessibilityDescription;
    }

    public void setSchoolAccessibilityDescription(String schoolAccessibilityDescription) {
        this.schoolAccessibilityDescription = schoolAccessibilityDescription;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolSports() {
        return schoolSports;
    }

    public void setSchoolSports(String schoolSports) {
        this.schoolSports = schoolSports;
    }

    public String getSeats101() {
        return seats101;
    }

    public void setSeats101(String seats101) {
        this.seats101 = seats101;
    }

    public String getSeats9ge1() {
        return seats9ge1;
    }

    public void setSeats9ge1(String seats9ge1) {
        this.seats9ge1 = seats9ge1;
    }

    public String getSeats9swd1() {
        return seats9swd1;
    }

    public void setSeats9swd1(String seats9swd1) {
        this.seats9swd1 = seats9swd1;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getSubway() {
        return subway;
    }

    public void setSubway(String subway) {
        this.subway = subway;
    }

    public String getTotalStudents() {
        return totalStudents;
    }

    public void setTotalStudents(String totalStudents) {
        this.totalStudents = totalStudents;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getAcademicopportunities3() {
        return academicopportunities3;
    }

    public void setAcademicopportunities3(String academicopportunities3) {
        this.academicopportunities3 = academicopportunities3;
    }

    public String getAddtlInfo1() {
        return addtlInfo1;
    }

    public void setAddtlInfo1(String addtlInfo1) {
        this.addtlInfo1 = addtlInfo1;
    }

    public String getEligibility1() {
        return eligibility1;
    }

    public void setEligibility1(String eligibility1) {
        this.eligibility1 = eligibility1;
    }

    public String getLanguageClasses() {
        return languageClasses;
    }

    public void setLanguageClasses(String languageClasses) {
        this.languageClasses = languageClasses;
    }

    public String getTransfer() {
        return transfer;
    }

    public void setTransfer(String transfer) {
        this.transfer = transfer;
    }

    public String getAcademicopportunities4() {
        return academicopportunities4;
    }

    public void setAcademicopportunities4(String academicopportunities4) {
        this.academicopportunities4 = academicopportunities4;
    }

    public String getAcademicopportunities5() {
        return academicopportunities5;
    }

    public void setAcademicopportunities5(String academicopportunities5) {
        this.academicopportunities5 = academicopportunities5;
    }

    public String getCollegeCareerRate() {
        return collegeCareerRate;
    }

    public void setCollegeCareerRate(String collegeCareerRate) {
        this.collegeCareerRate = collegeCareerRate;
    }

    public String getDiplomaendorsements() {
        return diplomaendorsements;
    }

    public void setDiplomaendorsements(String diplomaendorsements) {
        this.diplomaendorsements = diplomaendorsements;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getGirls() {
        return girls;
    }

    public void setGirls(String girls) {
        this.girls = girls;
    }

    public String getGraduationRate() {
        return graduationRate;
    }

    public void setGraduationRate(String graduationRate) {
        this.graduationRate = graduationRate;
    }

    public String getPsalSportsBoys() {
        return psalSportsBoys;
    }

    public void setPsalSportsBoys(String psalSportsBoys) {
        this.psalSportsBoys = psalSportsBoys;
    }

    public String getPsalSportsCoed() {
        return psalSportsCoed;
    }

    public void setPsalSportsCoed(String psalSportsCoed) {
        this.psalSportsCoed = psalSportsCoed;
    }

    public String getPsalSportsGirls() {
        return psalSportsGirls;
    }

    public void setPsalSportsGirls(String psalSportsGirls) {
        this.psalSportsGirls = psalSportsGirls;
    }

    public String getSharedSpace() {
        return sharedSpace;
    }

    public void setSharedSpace(String sharedSpace) {
        this.sharedSpace = sharedSpace;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getAdvancedplacementCourses() {
        return advancedplacementCourses;
    }

    public void setAdvancedplacementCourses(String advancedplacementCourses) {
        this.advancedplacementCourses = advancedplacementCourses;
    }

    public String getCampusName() {
        return campusName;
    }

    public void setCampusName(String campusName) {
        this.campusName = campusName;
    }

    public String getPrgdesc1() {
        return prgdesc1;
    }

    public void setPrgdesc1(String prgdesc1) {
        this.prgdesc1 = prgdesc1;
    }

    public String getAdmissionspriority41() {
        return admissionspriority41;
    }

    public void setAdmissionspriority41(String admissionspriority41) {
        this.admissionspriority41 = admissionspriority41;
    }

    public String getAdmissionspriority12() {
        return admissionspriority12;
    }

    public void setAdmissionspriority12(String admissionspriority12) {
        this.admissionspriority12 = admissionspriority12;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public String getGrade9geapplicants2() {
        return grade9geapplicants2;
    }

    public void setGrade9geapplicants2(String grade9geapplicants2) {
        this.grade9geapplicants2 = grade9geapplicants2;
    }

    public String getGrade9geapplicantsperseat2() {
        return grade9geapplicantsperseat2;
    }

    public void setGrade9geapplicantsperseat2(String grade9geapplicantsperseat2) {
        this.grade9geapplicantsperseat2 = grade9geapplicantsperseat2;
    }

    public String getGrade9gefilledflag2() {
        return grade9gefilledflag2;
    }

    public void setGrade9gefilledflag2(String grade9gefilledflag2) {
        this.grade9gefilledflag2 = grade9gefilledflag2;
    }

    public String getGrade9swdapplicants2() {
        return grade9swdapplicants2;
    }

    public void setGrade9swdapplicants2(String grade9swdapplicants2) {
        this.grade9swdapplicants2 = grade9swdapplicants2;
    }

    public String getGrade9swdapplicantsperseat2() {
        return grade9swdapplicantsperseat2;
    }

    public void setGrade9swdapplicantsperseat2(String grade9swdapplicantsperseat2) {
        this.grade9swdapplicantsperseat2 = grade9swdapplicantsperseat2;
    }

    public String getGrade9swdfilledflag2() {
        return grade9swdfilledflag2;
    }

    public void setGrade9swdfilledflag2(String grade9swdfilledflag2) {
        this.grade9swdfilledflag2 = grade9swdfilledflag2;
    }

    public String getInterest2() {
        return interest2;
    }

    public void setInterest2(String interest2) {
        this.interest2 = interest2;
    }

    public String getMethod2() {
        return method2;
    }

    public void setMethod2(String method2) {
        this.method2 = method2;
    }

    public String getPrgdesc2() {
        return prgdesc2;
    }

    public void setPrgdesc2(String prgdesc2) {
        this.prgdesc2 = prgdesc2;
    }

    public String getProgram2() {
        return program2;
    }

    public void setProgram2(String program2) {
        this.program2 = program2;
    }

    public String getSeats102() {
        return seats102;
    }

    public void setSeats102(String seats102) {
        this.seats102 = seats102;
    }

    public String getSeats9ge2() {
        return seats9ge2;
    }

    public void setSeats9ge2(String seats9ge2) {
        this.seats9ge2 = seats9ge2;
    }

    public String getSeats9swd2() {
        return seats9swd2;
    }

    public void setSeats9swd2(String seats9swd2) {
        this.seats9swd2 = seats9swd2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.academicopportunities1);
        dest.writeString(this.academicopportunities2);
        dest.writeString(this.admissionspriority11);
        dest.writeString(this.admissionspriority21);
        dest.writeString(this.admissionspriority31);
        dest.writeString(this.attendanceRate);
        dest.writeString(this.bbl);
        dest.writeString(this.bin);
        dest.writeString(this.boro);
        dest.writeString(this.borough);
        dest.writeString(this.buildingCode);
        dest.writeString(this.bus);
        dest.writeString(this.censusTract);
        dest.writeString(this.city);
        dest.writeString(this.code1);
        dest.writeString(this.communityBoard);
        dest.writeString(this.councilDistrict);
        dest.writeString(this.dbn);
        dest.writeString(this.directions1);
        dest.writeString(this.ellPrograms);
        dest.writeString(this.extracurricularActivities);
        dest.writeString(this.faxNumber);
        dest.writeString(this.finalgrades);
        dest.writeString(this.grade9geapplicants1);
        dest.writeString(this.grade9geapplicantsperseat1);
        dest.writeString(this.grade9gefilledflag1);
        dest.writeString(this.grade9swdapplicants1);
        dest.writeString(this.grade9swdapplicantsperseat1);
        dest.writeString(this.grade9swdfilledflag1);
        dest.writeString(this.grades2018);
        dest.writeString(this.interest1);
        dest.writeString(this.latitude);
        dest.writeString(this.location);
        dest.writeString(this.longitude);
        dest.writeString(this.method1);
        dest.writeString(this.neighborhood);
        dest.writeString(this.nta);
        dest.writeString(this.offerRate1);
        dest.writeString(this.overviewParagraph);
        dest.writeString(this.pctStuEnoughVariety);
        dest.writeString(this.pctStuSafe);
        dest.writeString(this.phoneNumber);
        dest.writeString(this.primaryAddressLine1);
        dest.writeString(this.program1);
        dest.writeString(this.requirement11);
        dest.writeString(this.requirement21);
        dest.writeString(this.requirement31);
        dest.writeString(this.requirement41);
        dest.writeString(this.requirement51);
        dest.writeString(this.school10thSeats);
        dest.writeString(this.schoolAccessibilityDescription);
        dest.writeString(this.schoolEmail);
        dest.writeString(this.schoolName);
        dest.writeString(this.schoolSports);
        dest.writeString(this.seats101);
        dest.writeString(this.seats9ge1);
        dest.writeString(this.seats9swd1);
        dest.writeString(this.stateCode);
        dest.writeString(this.subway);
        dest.writeString(this.totalStudents);
        dest.writeString(this.website);
        dest.writeString(this.zip);
        dest.writeString(this.academicopportunities3);
        dest.writeString(this.addtlInfo1);
        dest.writeString(this.eligibility1);
        dest.writeString(this.languageClasses);
        dest.writeString(this.transfer);
        dest.writeString(this.academicopportunities4);
        dest.writeString(this.academicopportunities5);
        dest.writeString(this.collegeCareerRate);
        dest.writeString(this.diplomaendorsements);
        dest.writeString(this.endTime);
        dest.writeString(this.girls);
        dest.writeString(this.graduationRate);
        dest.writeString(this.psalSportsBoys);
        dest.writeString(this.psalSportsCoed);
        dest.writeString(this.psalSportsGirls);
        dest.writeString(this.sharedSpace);
        dest.writeString(this.startTime);
        dest.writeString(this.advancedplacementCourses);
        dest.writeString(this.campusName);
        dest.writeString(this.prgdesc1);
        dest.writeString(this.admissionspriority41);
        dest.writeString(this.admissionspriority12);
        dest.writeString(this.code2);
        dest.writeString(this.grade9geapplicants2);
        dest.writeString(this.grade9geapplicantsperseat2);
        dest.writeString(this.grade9gefilledflag2);
        dest.writeString(this.grade9swdapplicants2);
        dest.writeString(this.grade9swdapplicantsperseat2);
        dest.writeString(this.grade9swdfilledflag2);
        dest.writeString(this.interest2);
        dest.writeString(this.method2);
        dest.writeString(this.prgdesc2);
        dest.writeString(this.program2);
        dest.writeString(this.seats102);
        dest.writeString(this.seats9ge2);
        dest.writeString(this.seats9swd2);
    }

    public School() {
    }

    protected School(Parcel in) {
        this.academicopportunities1 = in.readString();
        this.academicopportunities2 = in.readString();
        this.admissionspriority11 = in.readString();
        this.admissionspriority21 = in.readString();
        this.admissionspriority31 = in.readString();
        this.attendanceRate = in.readString();
        this.bbl = in.readString();
        this.bin = in.readString();
        this.boro = in.readString();
        this.borough = in.readString();
        this.buildingCode = in.readString();
        this.bus = in.readString();
        this.censusTract = in.readString();
        this.city = in.readString();
        this.code1 = in.readString();
        this.communityBoard = in.readString();
        this.councilDistrict = in.readString();
        this.dbn = in.readString();
        this.directions1 = in.readString();
        this.ellPrograms = in.readString();
        this.extracurricularActivities = in.readString();
        this.faxNumber = in.readString();
        this.finalgrades = in.readString();
        this.grade9geapplicants1 = in.readString();
        this.grade9geapplicantsperseat1 = in.readString();
        this.grade9gefilledflag1 = in.readString();
        this.grade9swdapplicants1 = in.readString();
        this.grade9swdapplicantsperseat1 = in.readString();
        this.grade9swdfilledflag1 = in.readString();
        this.grades2018 = in.readString();
        this.interest1 = in.readString();
        this.latitude = in.readString();
        this.location = in.readString();
        this.longitude = in.readString();
        this.method1 = in.readString();
        this.neighborhood = in.readString();
        this.nta = in.readString();
        this.offerRate1 = in.readString();
        this.overviewParagraph = in.readString();
        this.pctStuEnoughVariety = in.readString();
        this.pctStuSafe = in.readString();
        this.phoneNumber = in.readString();
        this.primaryAddressLine1 = in.readString();
        this.program1 = in.readString();
        this.requirement11 = in.readString();
        this.requirement21 = in.readString();
        this.requirement31 = in.readString();
        this.requirement41 = in.readString();
        this.requirement51 = in.readString();
        this.school10thSeats = in.readString();
        this.schoolAccessibilityDescription = in.readString();
        this.schoolEmail = in.readString();
        this.schoolName = in.readString();
        this.schoolSports = in.readString();
        this.seats101 = in.readString();
        this.seats9ge1 = in.readString();
        this.seats9swd1 = in.readString();
        this.stateCode = in.readString();
        this.subway = in.readString();
        this.totalStudents = in.readString();
        this.website = in.readString();
        this.zip = in.readString();
        this.academicopportunities3 = in.readString();
        this.addtlInfo1 = in.readString();
        this.eligibility1 = in.readString();
        this.languageClasses = in.readString();
        this.transfer = in.readString();
        this.academicopportunities4 = in.readString();
        this.academicopportunities5 = in.readString();
        this.collegeCareerRate = in.readString();
        this.diplomaendorsements = in.readString();
        this.endTime = in.readString();
        this.girls = in.readString();
        this.graduationRate = in.readString();
        this.psalSportsBoys = in.readString();
        this.psalSportsCoed = in.readString();
        this.psalSportsGirls = in.readString();
        this.sharedSpace = in.readString();
        this.startTime = in.readString();
        this.advancedplacementCourses = in.readString();
        this.campusName = in.readString();
        this.prgdesc1 = in.readString();
        this.admissionspriority41 = in.readString();
        this.admissionspriority12 = in.readString();
        this.code2 = in.readString();
        this.grade9geapplicants2 = in.readString();
        this.grade9geapplicantsperseat2 = in.readString();
        this.grade9gefilledflag2 = in.readString();
        this.grade9swdapplicants2 = in.readString();
        this.grade9swdapplicantsperseat2 = in.readString();
        this.grade9swdfilledflag2 = in.readString();
        this.interest2 = in.readString();
        this.method2 = in.readString();
        this.prgdesc2 = in.readString();
        this.program2 = in.readString();
        this.seats102 = in.readString();
        this.seats9ge2 = in.readString();
        this.seats9swd2 = in.readString();
    }

    public static final Parcelable.Creator<School> CREATOR = new Parcelable.Creator<School>() {
        @Override
        public School createFromParcel(Parcel source) {
            return new School(source);
        }

        @Override
        public School[] newArray(int size) {
            return new School[size];
        }
    };
}
