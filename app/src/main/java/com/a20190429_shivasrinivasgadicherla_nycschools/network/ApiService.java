package com.a20190429_shivasrinivasgadicherla_nycschools.network;

import com.a20190429_shivasrinivasgadicherla_nycschools.model.School;
import com.a20190429_shivasrinivasgadicherla_nycschools.model.SchoolDetails;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by srini on 4/30/19.
 */

public interface ApiService {

    @GET("s3k6-pzi2.json")
    Call<List<School>> getSchoolsList();

    @GET("f9bf-2cp4.json")
    Call<List<SchoolDetails>> getSchoolDetails();
}
