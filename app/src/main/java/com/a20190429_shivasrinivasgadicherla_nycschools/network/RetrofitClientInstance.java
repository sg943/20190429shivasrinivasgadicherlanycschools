package com.a20190429_shivasrinivasgadicherla_nycschools.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by srini on 4/30/19.
 */

public class RetrofitClientInstance {

    private static Retrofit retrofit;
    private static Gson gson;
    private static final String BASE_URL = "https://data.cityofnewyork.us/resource/";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

}
