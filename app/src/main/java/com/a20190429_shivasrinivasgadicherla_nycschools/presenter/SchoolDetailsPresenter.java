package com.a20190429_shivasrinivasgadicherla_nycschools.presenter;

import android.content.Context;

import com.a20190429_shivasrinivasgadicherla_nycschools.interactor.SchoolDetailsInteractor;
import com.a20190429_shivasrinivasgadicherla_nycschools.model.School;
import com.a20190429_shivasrinivasgadicherla_nycschools.model.SchoolDetails;
import com.a20190429_shivasrinivasgadicherla_nycschools.network.ApiService;
import com.a20190429_shivasrinivasgadicherla_nycschools.network.RetrofitClientInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by srini on 4/30/19.
 */

public class SchoolDetailsPresenter {

    private SchoolDetailsInteractor interactor;
    private School currentSchool;

    public SchoolDetailsPresenter(Context context) {
        if(context instanceof SchoolDetailsInteractor) {
            interactor = (SchoolDetailsInteractor) context;
        }
    }

    public void getSchoolDetails(School school) {
        this.currentSchool = school;
        getSchoolDetails();
    }

    private void getSchoolDetails() {
        ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
        Call<List<SchoolDetails>> call = service.getSchoolDetails();
        call.enqueue(new Callback<List<SchoolDetails>>() {
            @Override
            public void onResponse(Call<List<SchoolDetails>> call, Response<List<SchoolDetails>> response) {
                onGetSchoolsDetailsSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<SchoolDetails>> call, Throwable t) {
                onGetSchoolsDetailsFailure();
            }
        });
    }

    private void onGetSchoolsDetailsSuccess(List<SchoolDetails> schoolDetailsList) {
        SchoolDetails schoolDetails = null;

        for(SchoolDetails details : schoolDetailsList) {
            if(details.getDbn().equalsIgnoreCase(currentSchool.getDbn())) {
                schoolDetails = details;
                break;
            }
        }

        if(interactor != null && schoolDetails != null) {
            interactor.setData(schoolDetails);
        }
    }

    private void onGetSchoolsDetailsFailure() {
        if(interactor != null) {
            interactor.showError();
        }
    }
}
