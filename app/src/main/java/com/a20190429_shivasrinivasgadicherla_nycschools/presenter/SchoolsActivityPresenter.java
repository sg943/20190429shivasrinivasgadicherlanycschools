package com.a20190429_shivasrinivasgadicherla_nycschools.presenter;

import android.content.Context;

import com.a20190429_shivasrinivasgadicherla_nycschools.interactor.SchoolsActivityInteractor;
import com.a20190429_shivasrinivasgadicherla_nycschools.model.School;
import com.a20190429_shivasrinivasgadicherla_nycschools.network.ApiService;
import com.a20190429_shivasrinivasgadicherla_nycschools.network.RetrofitClientInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by srini on 4/30/19.
 */

public class SchoolsActivityPresenter {

    SchoolsActivityInteractor interactor;

    public SchoolsActivityPresenter(Context context) {
        if(context instanceof SchoolsActivityInteractor) {
            interactor = (SchoolsActivityInteractor) context;
        }
    }

    public void getSchools() {
        ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
        Call<List<School>> call = service.getSchoolsList();
        call.enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                onGetSchoolsSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                onGetSchoolsFailure();
            }
        });
    }

    private void onGetSchoolsSuccess(List<School> schoolsList) {
        if(interactor != null) {
            interactor.setData(schoolsList);
        }
    }

    private void onGetSchoolsFailure() {
        if(interactor != null) {
            interactor.showError();
        }
    }

}
