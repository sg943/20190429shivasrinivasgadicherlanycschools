package com.a20190429_shivasrinivasgadicherla_nycschools.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.a20190429_shivasrinivasgadicherla_nycschools.R;
import com.a20190429_shivasrinivasgadicherla_nycschools.interactor.SchoolDetailsInteractor;
import com.a20190429_shivasrinivasgadicherla_nycschools.model.School;
import com.a20190429_shivasrinivasgadicherla_nycschools.model.SchoolDetails;
import com.a20190429_shivasrinivasgadicherla_nycschools.presenter.SchoolDetailsPresenter;
import com.a20190429_shivasrinivasgadicherla_nycschools.utility.Constants;

/**
 * Created by srini on 4/30/19.
 */

public class SchoolDetailsActivity extends AppCompatActivity implements SchoolDetailsInteractor {

    private SchoolDetailsPresenter presenter;
    private TextView tvDbn;
    private TextView tvSchoolName;
    private TextView tvTestTakers;
    private TextView tvReadingAvg;
    private TextView tvWritingAvg;
    private TextView tvMath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("School Details");

        presenter = new SchoolDetailsPresenter(this);
        School school = getIntent().getParcelableExtra(Constants.INTENT_KEY_SCHOOL);

        tvDbn = (TextView) findViewById(R.id.tv_school_dbn_value);
        tvReadingAvg = (TextView) findViewById(R.id.tv_school_reading_avg_value);
        tvWritingAvg = (TextView) findViewById(R.id.tv_school_writing_avg_value);
        tvTestTakers = (TextView) findViewById(R.id.tv_school_test_takers_value);
        tvSchoolName = (TextView) findViewById(R.id.tv_school_name);
        tvMath = (TextView) findViewById(R.id.tv_school_math_avg_value);

        presenter.getSchoolDetails(school);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter = null;
    }

    @Override
    public void setData(SchoolDetails schoolDetails) {
        tvDbn.setText(schoolDetails.getDbn());
        tvSchoolName.setText(schoolDetails.getSchoolName());
        tvTestTakers.setText(schoolDetails.getNumOfSatTestTakers());
        tvReadingAvg.setText(schoolDetails.getSatCriticalReadingAvgScore());
        tvWritingAvg.setText(schoolDetails.getSatWritingAvgScore());
        tvMath.setText(schoolDetails.getSatMathAvgScore());
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.error_unable_to_get_schools, Toast.LENGTH_LONG).show();
    }
}
