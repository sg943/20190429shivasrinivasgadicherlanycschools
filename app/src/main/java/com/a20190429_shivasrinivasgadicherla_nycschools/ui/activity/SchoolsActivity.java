package com.a20190429_shivasrinivasgadicherla_nycschools.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.a20190429_shivasrinivasgadicherla_nycschools.R;
import com.a20190429_shivasrinivasgadicherla_nycschools.interactor.SchoolsActivityInteractor;
import com.a20190429_shivasrinivasgadicherla_nycschools.model.School;
import com.a20190429_shivasrinivasgadicherla_nycschools.presenter.SchoolsActivityPresenter;
import com.a20190429_shivasrinivasgadicherla_nycschools.ui.adapter.SchoolsListAdapter;
import com.a20190429_shivasrinivasgadicherla_nycschools.utility.Constants;

import java.util.List;

public class SchoolsActivity extends AppCompatActivity implements SchoolsActivityInteractor,
        SchoolsListAdapter.SchoolsListListener {

    private SchoolsActivityPresenter presenter;
    private RecyclerView rvSchoolsList;
    private List<School> schoolsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schools);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Schools");

        presenter = new SchoolsActivityPresenter(this);

        rvSchoolsList = findViewById(R.id.rv_schools_list);
        rvSchoolsList.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvSchoolsList.setLayoutManager(layoutManager);

        presenter.getSchools();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter = null;
    }

    @Override
    public void setData(List<School> schoolsList) {
        this.schoolsList = schoolsList;
        SchoolsListAdapter adapter = new SchoolsListAdapter(this, schoolsList);
        rvSchoolsList.setAdapter(adapter);
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.error_unable_to_get_schools, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(int position) {
        onItemSelected(schoolsList.get(position));
    }

    private void onItemSelected(School school) {
        Intent schoolDetailsIntent = new Intent(SchoolsActivity.this, SchoolDetailsActivity.class);
        schoolDetailsIntent.putExtra(Constants.INTENT_KEY_SCHOOL, school);
        startActivity(schoolDetailsIntent);
    }
}
