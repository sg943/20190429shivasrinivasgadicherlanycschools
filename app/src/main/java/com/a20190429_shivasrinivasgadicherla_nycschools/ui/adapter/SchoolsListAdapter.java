package com.a20190429_shivasrinivasgadicherla_nycschools.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.a20190429_shivasrinivasgadicherla_nycschools.R;
import com.a20190429_shivasrinivasgadicherla_nycschools.model.School;

import java.util.List;

/**
 * Created by srini on 4/30/19.
 */

public class SchoolsListAdapter extends RecyclerView.Adapter<SchoolsListAdapter.ViewHolder> {

    private List<School> schoolsList;
    private SchoolsListListener listener;

    public SchoolsListAdapter(Context context, List<School> schoolsList) {
        this.schoolsList = schoolsList;
        listener = (SchoolsListListener) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_school, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final School currentSchool = schoolsList.get(position);
        holder.tvSchoolName.setText(currentSchool.getSchoolName());
        holder.tvSchoolDescription.setText(currentSchool.getAcademicopportunities1());
        holder.tvSchoolCity.setText(currentSchool.getCity());

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null) {
                    listener.onItemClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return schoolsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvSchoolName;
        private TextView tvSchoolDescription;
        private TextView tvSchoolCity;
        private View root;

        public ViewHolder(View itemView) {
            super(itemView);

            tvSchoolName = (TextView) itemView.findViewById(R.id.tv_school_name);
            tvSchoolDescription = (TextView) itemView.findViewById(R.id.tv_school_description);
            tvSchoolCity = (TextView) itemView.findViewById(R.id.tv_school_city);
            root = itemView;
        }
    }

    public interface SchoolsListListener {
        public void onItemClick(int position);
    }

}
