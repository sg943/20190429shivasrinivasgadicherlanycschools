package com.a20190429_shivasrinivasgadicherla_nycschools.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.a20190429_shivasrinivasgadicherla_nycschools.R;

/**
 * Created by srini on 4/30/19.
 */

public class SchoolsListFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_schools_list, container, false);
    }

}
